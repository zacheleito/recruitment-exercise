
<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="/themes/creditstar_ee/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/themes/creditstar_ee/img/favicon.ico" type="image/x-icon">
    <link rel="icon" sizes="196x196" href="/themes/creditstar_ee/img/creditstar-icon-196x196.png">
    <link rel="icon" sizes="128x128" href="/themes/creditstar_ee/img/creditstar-icon-128x128.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/themes/creditstar_ee/img/creditstar-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/themes/creditstar_ee/img/creditstar-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/themes/creditstar_ee/img/creditstar-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/themes/creditstar_ee/img/creditstar-icon-57x57.png" />
<!--    <meta name="viewport" content="width=1024">-->
    <link rel="canonical" href="https://www.creditstar.ee/" />
<link hreflang="et-ee" rel="alternate" href="https://www.creditstar.ee/" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="/themes/creditstar_ee/js/product/annuity.js?ver=20180530"></script>
<script type="text/javascript" src="/themes/creditstar_ee/js/product/base-product.js?ver=20180530"></script>
<script type="text/javascript" src="/themes/creditstar_ee/js/product/product.js?ver=20180530"></script>
<script type="text/javascript" src="/themes/creditstar_ee/js/calculator-fixed-apr.js?ver=20190110"></script>
<title>Personaalne laenukonto | Creditstar</title>
            <meta name="description" content="Creditstari personaalne laenukonto on justkui virtuaalne krediitkaart, mis annab kindlustunde ning tagavararaha.">
        <!--[if gte IE 9]><!--><link rel="stylesheet" href="/themes/creditstar_ee/css/foundation.css?ver=20160509"/><!--<![endif]-->
    <!--[if !IE]><!--><link rel="stylesheet" href="/themes/creditstar_ee/css/foundation.css?ver=20160509"/><!--<![endif]-->

    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&subset=latin,latin-ext,cyrillic,cyrillic-ext'>
    <link rel="stylesheet" href="/themes/creditstar_ee/css/jquery.nouislider.css"/>
    <link rel="stylesheet" href="/themes/creditstar_ee/css/smslaen.css?ver=20160813"/>
    <link rel="stylesheet" href="/themes/creditstar_ee/css/creditstar.css?ver=20190121"/>
    <link rel="stylesheet" href="/themes/creditstar_ee/css/addtohomescreen.css"/>
    <script type="text/javascript">

        (function(document,navigator,standalone) {
            // prevents links from apps from oppening in mobile safari
            // this javascript must be the first script in your <head>
            if ((standalone in navigator) && navigator[standalone]) {
                var curnode, location=document.location, stop=/^(a|html)$/i;
                document.addEventListener('click', function(e) {
                    curnode=e.target;
                    while (!(stop).test(curnode.nodeName)) {
                        curnode=curnode.parentNode;
                    }
                    // Condidions to do this only on links to your own app
                    // if you want all links, use if('href' in curnode) instead.
                    if('href' in curnode && ( curnode.href.indexOf('http') || ~curnode.href.indexOf(location.host) ) ) {
                        if(curnode.id != 'teabeleht') {
                            e.preventDefault();
                            location.href = curnode.href;
                        }
                    }
                },false);
            }
        })(document,window.navigator,'standalone');

        window.CURRENT_LOCALE = 'et';
            </script>
    <script src="/themes/creditstar_ee/js/modernizr.js"></script>
    <script src="/themes/creditstar_ee/js/vendor/jquery.tinyscrollbar.min.js"></script>

    <!--[if lt IE 9]>
    <script src="https://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <link rel="stylesheet" href="/themes/creditstar_ee/css/foundation_ie8.css"/>
    <![endif]-->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-2915053-1', 'auto');
        ga('require', 'displayfeatures');
        ga('require', 'linkid', 'linkid.js');
                ga('send', 'pageview');

    </script>
        <!-- SMS Laen Clickit Retargeting EST -->
    <script type="text/javascript"><!--<![CDATA[
        /* (c)AdOcean 2003-2014 */
        /* PLACEMENT: mbd_ee.Clickit Retargetings.SMSLaen.Activation EST */
        if(location.protocol.substr(0,5)=='https')document.write(unescape('%3C')+'script id="mbd_ee.Clickit Retargetings.SMSLaen.Activation EST" src="'+location.protocol+'//ee.adocean.pl/_'+(new Date()).getTime()+'/ad.js?id=qwfnILeCd67ve7we_647epgXzvg4I4_k8r10_GLDqaj.h7/x='+screen.width+'/y='+screen.height+'" type="text/javascript"'+unescape('%3E%3C')+'/script'+unescape('%3E'));
        //]]>--></script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '805618629498397');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=805618629498397&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->

	<!-- Facebook Conversion Code for Smslaen.ee all visits pixel -->
	<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
				var fbds = document.createElement('script');
				fbds.async = true;
				fbds.src = '//connect.facebook.net/en_US/fbds.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(fbds, s);
				_fbq.loaded = true;
			}
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', '6024147685946', {'value':'0.00','currency':'EUR'}]);
	</script>
	<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6024147685946&amp;cd[value]=0.00&amp;cd[currency]=EUR&amp;noscript=1" /></noscript>

</head>
<body class="lang-et">
<div id="page-header" class="contain-to-grid hide-for-print">
    <div class="top-bar" data-topbar>
        <section class="top-bar-section">
            <ul class="customer-service">
                <li>Klienditeenindus</li>
                <li class="customerservice-icon"><img src="/themes/creditstar_ee//img/ico-customerservice.png" alt=""/>+372 644 0467</li>
                <li class="openingtimes-icon"><img src="/themes/creditstar_ee//img/ico-openingtimes.png" alt=""/>E-P 9.00-21.00</li>
            </ul>
        </section>
        <section class="top-bar-section client-area">
            <ul class="right">
                                    <li><a class="button-orange" href="/est/site/login"><img src="/themes/creditstar_ee//img/ico-login.png" alt=""
                                                                                                    style="padding-right: 6px;margin-top: -4px;"/>LOGI SISSE</a>
                    </li>
                    <li><a class="button-orange" href="/est/register/index">AVA KONTO</a></li>
                            </ul>
        </section>
    </div>
</div>
<div id="main-menu" class="contain-to-grid down-shadow  hide-for-print">
    <nav class="top-bar" data-topbar>
        <ul class="title-area">
            <li class="name">
                <h1>
                    <a href="/">

                        
                                                    <img src="/themes/creditstar_ee//img/header-logo.png" alt="Creditstar"/>

                            
                        
                        <span class="invisible">Creditstar</span>
                    </a>
                </h1>
            </li>
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menüü</span></a></li>
        </ul>
        <section class="top-bar-section">
            <ul class="right">
                <li class="language-change">
                    <a href="/rus">По-русски</a></li>
            </ul>
            <ul class="left" id="yw0">
<li><a href="/est/site/laenud">Laenud</a></li>
<li><a href="/est/site/help">Vajad abi?</a></li>
<li><a href="/est/site/firmast">Firmast</a></li>
<li><a href="/est/site/vastutustundlik">Vastutustundlik laenamine</a></li>
</ul>        </section>
    </nav>

    </div>
<div class="page-wrapper">

    <div id="content">
	<div id="front-banner">
    <div id="testimonials">

        <div class="slide" >
            <div class="testimonial-quote">
                <div id="front-banner-slogan" class="row">
                    <div id="front-background-one" class="front-background">
                        <div id="creditstar-july-2019" class="front-banner-background animated"></div>
                    </div>
                    <div id="front-background-one-slogan" class="slogan">
                        <div id="slogan-june-2017" class="medium-12">
                            <div class="small-10 columns">
                                <h2 class="heading-one">
                                Muuda oma <br>unistused<br> reaalsuseks<br>
                                    <!--Suvi on kohal!<br>Võta suvest viimast<br>Creditstariga<br>--> </h2>
                                <!--<h2 class="heading-one-one">
                                    Uue aasta võimalused <br>Creditstari laenukontol.<br> </h2>--
                                <!--<h2 class="heading-four">
                                    Tagavararaha on Sinu jaoks olemas,<br> kui peaksid seda vajama.
                                </h2>
                                <h2 class="heading-four-one">
                                    tagavararaha on Sinu <br>jaoks olemas, kui <br>peaksid seda vajama.
                                </h2>-->
                                <h2 class="heading-three">
                                    <ul>
                                        <li>Personaalne laenukonto kuni 5000 €</li>
                                        <li>Digitaalne isikutuvastus</li>
                                        <li>Kiire taotluse protsess</li>
                                    </ul>
                                </h2>
                                <!--<h2 class="heading-three-one">
                                    <ul>
                                        <li>Kuni 5000€</li>
                                        <li>Kiire taotluse protsess</li>
                                        <li>Vastus 10 minutiga</li>
                                    </ul>
                                    <div id="bullet">Kuni 5000€ eriliste mälestuste loomiseks</div>
                                    <div id="bullet">Kiire taotluse protsess</div>
                                    <div id="bullet">Vastus 10 minutiga</div>
                                </h2>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        /*
         //for changing banner in first page
         setInterval(function () {
         jQuery('#testimonials .slide').filter(':visible').fadeOut(3000, function () {
         if (jQuery(this).next('.slide').size()) {
         jQuery(this).next().fadeIn(3000);
         }
         else {
         jQuery('#testimonials .slide').eq(0).fadeIn(3000);
         }
         });
         }, 9000);*/
    </script>
    <div style="clear: both"></div>

    <div id="front-banner-slider" class="row calculator-wrapper">
        <div class="medium-12 columns">
            <form action="/est/user/loan" method="post" style="display: none;"
                  id="LoanCalc_form">
                <input type="hidden" name="LoanCalc[sum]" id="LoanCalc_sum"/>
                <input type="hidden" name="LoanCalc[period]" id="LoanCalc_period"/>
                <input type="hidden" name="LoanCalc[clicked]" id="LoanCalc_clicked" value="0"/>
            </form>
            <div class="row calculator-header">
                <div class="show-for-small-only calc-helptext">
                    Vali sobiv laenusumma ja periood. Kui oled uus klient, avame Sulle tasuta personaalse laenukonto.
                </div>
                <div class="show-for-medium-up calc-helptext">
                    Vali sobiv laenusumma ja periood. Kui oled uus klient, avame Sulle tasuta personaalse laenukonto.
                </div>
            </div>
            <div class="row calculator-body">
                <div class="medium-7 columns">
                    <div class="slider-container">
                        <div class="row slider-heading-row">
                            <div class="medium-6 small-6 columns slider-title">Summa</div>
                            <div class="medium-6 small-6 columns slider-value text-right">
                                <span id="sum-display"></span>
                                <img id="calc-sum-minus" src="/themes/creditstar_ee//img/calculator-minus.png" alt="-"/>
                                <img id="calc-sum-plus" src="/themes/creditstar_ee//img/calculator-plus.png" alt="+"/>
                            </div>
                        </div>
                        <div class="row slider-handle-row">
                            <div id="money-slider"></div>
                        </div>
                        <div class="row slider-extrainfo-row">
                            <div class="medium-6 small-6 columns amount">50 €</div>
                            <!-- fack-->
                            <div class="medium-6 small-6 columns text-right"><span
                                    id="loan-display-maxsum">5000 €</span></div>
                        </div>
                    </div>
                    <div class="slider-container">
                        <div class="row slider-heading-row">
                            <div class="medium-6 small-5 columns slider-title">Periood</div>
                            <div class="medium-6 small-7 columns slider-value text-right">
                                <span id="period-display"></span>
                                <img id="calc-period-minus" src="/themes/creditstar_ee//img/calculator-minus.png"
                                     alt="-"/>
                                <img id="calc-period-plus" src="/themes/creditstar_ee//img/calculator-plus.png" alt="+"/>
                            </div>
                        </div>
                        <div class="row slider-handle-row">
                            <div id="period-slider"></div>
                        </div>
                        <div class="row slider-extrainfo-row">
                            <div class="medium-6 small-6 columns period">30 päeva</div>
                            <div class="medium-6 small-6 columns text-right">36 kuud</div>
                        </div>
                    </div>
                </div>
                <div class="medium-2 columns text-center show-for-medium-up">
                    <div style="display: table; height: 220px; width: 100%;">
                        <div id="select-loan-button"
                             style="display: table-cell; vertical-align: middle; cursor: pointer; position: relative;">
                            <img src="/themes/creditstar_ee//img/calculator-ee-select-over.png" alt="Vali"
                                 style="position:absolute;margin-top:-50%;left:0;"/>
                            <img id="select-loan-button-over" src="/themes/creditstar_ee//img/calculator-ee-select.png"
                                 alt="Vali"
                                 style="position:absolute;margin-top:-50%;left:0;display: none;"/>
                        </div>
                    </div>
                </div>
                <div class="medium-3 columns" style="padding: 0;">
                    <div style="display: table; height: 220px; width: 95%;">
                        <div style="display: table-cell; vertical-align: middle;" class="calc-loan-info">
                            <div class="row title">Laenu info</div>
                            <div class="row info-row">
                                <div class="small-6 medium-7 columns info-row-title text-right">Summa</div>
                                <div class="small-6 medium-5 columns info-row-value text-left"><span
                                        id="loan-display-sum"></span></div>
                            </div>
                            <div class="row info-row">
                                <div class="small-6 medium-7 columns info-row-title text-right">Kuumakse</div>
                                <div class="small-6 medium-5 columns info-row-value text-left"><span
                                        id="loan-display-payment"></span></div>
                            </div>
                            <div class="row info-row">
                                <div class="small-6 medium-7 columns info-row-title text-right">Periood</div>
                                <div class="small-6 medium-5 columns info-row-value text-left"><span
                                        id="loan-display-period"></span></div>
                            </div>
                            <div class="row text-center">
                                <a class="teabeleht" id="teabeleht">Krediidi teabeleht</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="show-for-small-only small-12 columns">
                    <div style="display: table; height: 150px; width: 100%;">
                        <div id="select-loan-button-small"
                             style="display: table-cell; vertical-align: middle; cursor: pointer; position: relative;">
                            <img src="/themes/creditstar_ee//img/calculator-ee-select-mobile-over.png" alt="Vali"
                                 style="position:absolute;top:20px;left:50%;margin-left:-130px;"/>
                            <img id="select-loan-button-over-small"
                                 src="/themes/creditstar_ee//img/calculator-ee-select-mobile.png" alt="Vali"
                                 style="position:absolute;top:20px;left:50%;display: none;margin-left:-130px;"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row fi-disclaimer">
        <div class="medium-12 columns">
            <img src="/themes/creditstar_ee//img/lion.png" alt="Lion" style="margin-top: -3px;"/>
            Creditstar Estonia tegutseb Finantsinspektsiooni poolt väljastatud krediidiandja
            <a href="http://www.fi.ee/index.php?id=18372&action=showentity&eid=104043" target="_blank"> tegevusloa</a>
            alusel.
        </div>
    </div>
</div><div id="paindlikult" class="white-block">
    <div class="row">
        <div class="medium-12 columns medium-centered">
            <h2 class="text-center">Personaalne laenukonto annab kindlustunde ning tagavararaha.</h2>
            <div class="row">
                <div class="medium-8 columns text-center push-2 light-text medium-line push-down">
                    Võimaldame finantsteenust, kus Sina kontrollid paindlikult kõiki otsuseid, mis puudutavad laenukonto
                    kasutamist. Meie online krediidi teenuse puhul võid Sina:
                </div>
            </div>
            <div class="row">
                <div class="medium-4 medium-4 columns">
                    <div class="iconed-text">
                        <div class="icon ico-1">&nbsp;</div>
                        <p>Kasutada oma laenukontot kas täielikult või osaliselt</p>
                    </div>
                </div>
                <div class="medium-4 medium-4 columns">
                    <div class="iconed-text">
                        <div class="icon ico-2">&nbsp;</div>
                        <p>Kasutuses oleva laenusumma ennetähtaegselt ja seeläbi soodsamalt tagastada</p>
                    </div>
                </div>
                <div class="medium-4 medium-4 columns push-down">
                    <div class="iconed-text">
                        <div class="icon ico-3">&nbsp;</div>
                        <p>Pikendada laenusumma tagastamise tähtaega</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row text-center" id="front-content-toggler-container">
        <div class="text-center title">Meie laenukontost, taotlemisest ja ettevõttest</div>
        <a id="front-content-toggler" href="#front-initial-hidden">Rohkem infot</a>
    </div>

    <div class="row text-center">
        <div id="front-kliendid-kiidavad" class="medium-8 medium-push-2 columns">
            <div class="image medium-12 columns">
            	<!--<a href="http://www.heateenindus.ee/kiidan/?firma=104" target="_blank">-->
                    <img style="max-width:100%;max-height:100%;"
                         src="/themes/creditstar_ee//img/Kliendid-kiidavad-EST_over.jpg" alt="Hea teenindus"/>
                <!--</a>-->
            </div>
        </div>
    </div>
</div>

<div id="front-initial-hidden" style="display: none;">
    <div class="dotted-line">&nbsp;</div>
    <div id="front-kuidas" class="gray-block">
        <div class="row">
            <div class="medium-12 columns medium-centered">
                <h2 class="text-center">Kuidas toimib personaalse laenukonto kasutamine?</h2>

                <div class="row">
                    <div class="medium-8 columns push-2">
                        <div class="medium-12 columns">
                            <div class="icon-padding"><img src="/themes/creditstar_ee//img/kuidastoimib-ico-sliders.png"
                                                           alt=""/></div>
                            <div class="front-kuidas-mobile-h3-container"><h3 id="front-kuidas-special-h3">Esita taotlus
                                    laenukonto koheseks kasutamiseks. <br/>Vali selleks <span class="orange"> sobiv laenusumma ja -periood.</span>
                                </h3></div>
                        </div>
                        <div class="medium-12 columns">
                            <div class="icon-padding"><img src="/themes/creditstar_ee//img/kuidastoimib-ico-vali.png"
                                                           alt=""/></div>
                            <h3>Kinnita tehtud valik nupule<span class="orange"> Vali</span> vajutades.</h3>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="step-arrow-down">SEEJÄREL <span style="font-size: 25px;">...</span></div>
                </div>
                <div class="row">
                    <div class="medium-5 medium-5 push-1 columns">
                        <div class="row">
                            <div class="medium-12 columns">
                                <div class="icon-padding"><img src="/themes/creditstar_ee//img/ico-a.png" alt=""/></div>
                                <p>Logi sisse ja kinnita, et nõustud laenukonto kasutamise tingimustega. See võtab aega
                                    ainult mõned minutid.</p>
                            </div>
                        </div>
                    </div>
                    <div class="medium-6 medium-6 columns">
                        <div class="row">
                            <div class="medium-12 columns">
                                <div class="icon-padding" style="min-height:130px;"><img
                                            src="/themes/creditstar_ee//img/ico-b.png" alt=""/></div>
                                <p>
                                    Ava meie veebikeskonnas endale tasuta personaalne laenukonto ja siis kinnita
                                    lõplikult
                                    laenukonto kasutamise soov ja tingimustega nõustumine. See toiming ei kesta kauem
                                    kui 10 minutit.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dotted-line">&nbsp;</div>
    <div id="front-meie" class="white-block">
        <div class="row push-down-40">
            <div class="medium-12 medium-12 columns">
                <div class="row">
                    <div class="medium-8 columns">
                        <h4><span class="orange">LAENUKONTO KASUTAMINE</span> on sobiv lahendus ettenägematute kulude
                            katteks,
                            kaupade soetamiseks, tervisega seotud kuludeks, reisimiseks, hobidega tegelemiseks ja
                            paljudeks muudeks vajadusteks.
                        </h4>

                        <p>Võimaldame kasutada laenukontot maksimaalselt 2 000 € ulatuses, tähtajaga kuni 1 aasta. Kui
                            oled saanud positiivse otsuse laenukonto kasutamiseks,
                            on Sul tagavararaha soodsateks ostuvõimalusteks ja ootamatuteks väljaminekuteks. Just siis
                            kui kõige rohkem vaja.</p>
                    </div>
                    <div class="medium-4 columns push-down"><img
                                src="/themes/creditstar_ee//img/icos-front-collection.png" alt=""/></div>
                </div>
                <div class="row">
                    <div class="medium-6 medium-6 columns">
                        <h2><img src="/themes/creditstar_ee//img/ico-front-teenus.png" alt=""
                                 style="padding-right: 10px;"/><span class="orange">MEIE TEENUS</span></h2>

                        <p>Oleme klientide jaoks kättesaadavad kiirelt, mugavalt ja paindlikult - e-teeninduse,
                            mobiilisõnumite ja koostööpartnerite
                            vahendusel.</p>
                    </div>
                    <div class="medium-6 medium-6 columns push-down">
                        <h2><img src="/themes/creditstar_ee//img/ico-front-kliendid.png" alt=""
                                 style="padding-right: 10px;"/><span class="orange">MEIE KLIENDID</span></h2>

                        <p>Kliendid hindavad meie teenust kõrgelt. Rahvusvaheliselt usaldab Creditstari üle <span class="orange">500 000</span> kliendi, kellest enam kui kaks kolmandikku on aktiivsed teenuse kasutajad.</p>

                    </div>
                </div>
            </div>
            <div class="row text-center">
                <a class="button-light" href="/est/site/laenud">Loe lähemalt personaalse
                    laenukonto kohta</a>
            </div>
        </div>
    </div>
    <div class="dotted-line">&nbsp;</div>
    <div id="front-ettevote" class="white-block">
        <div class="row push-down-40">
            <h2 class="text-center">Creditstar on suurim online krediidi pakkuja Eestis.</h2>

            <div class="row">
                <div class="medium-6 medium-6 columns">
                    <h3 class="ico-paindlik">ESIMENE JA UUENDUSLIK</h3>

                    <p>Alustasime 2006. aastal Eestis, kaubamärgiga SMS Laen. Oleme olnud üheks pangavälise
                        väikefinantseerimise teerajajaks,
                        võimaldades esimestena oma klientidele erinevaid tootelahendusi.</p>
                </div>
                <div class="medium-6 medium-6 columns push-down">
                    <h3 class="ico-pikaajalisem">RAHVUSVAHELINE FINANTSEERIMISETTEVÕTE</h3>

                    <p>Creditstar on täna rahvusvaheline online krediiditooteid pakkuv ettevõte,
                        mis ühendab oma teenustes klientide vajadused kaasaegsete tehnoloogiliste lahendustega. </p>
                </div>
            </div>
            <div class="row text-center">
                <a class="button-light" href="/est/site/firmast">Loe meie ettevõtte kohta
                    lähemalt</a>
            </div>
        </div>
    </div>
    <div class="row push-down-40"></div>

    <div id="taotle-footer" class="contain-to-grid pull-down-40 text-center">
    <div class="row">
        <a class="button-orange button-large" href="/est/user/loan">
            <p>TAOTLE LAENUKONTO KASUTAMIST</p>
        </a>

        <a href="javascript:void(0);" class="go-up button-orange button-large" onclick="scrollToTop();">
            <p>
                <img src="/themes/creditstar_ee//img/arrow-to-top-icon.png">
            </p>
        </a>
    </div>
</div></div>

<script>
    $(function () {
        $('#front-content-toggler').click(function (evt) {
            evt.preventDefault();
            $('#front-initial-hidden').slideToggle();

            $('#front-content-toggler').fadeOut(200, function () {
                $('#front-content-toggler').toggleClass('active');
                if ($('#front-content-toggler').hasClass('active')) {
                    $('#front-content-toggler').html('Sulge');
                } else {
                    $('#front-content-toggler').html('Rohkem infot');
                }
                $(this).fadeIn();
            });
        });
        if (!(navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape')) {
            $("#select-loan-button img").each(function () {
                $(this).css("top", "50%");
            });
        }
        $('#select-loan-button, #select-loan-button-small').hover(function () {
            $('#select-loan-button-over, #select-loan-button-over-small').fadeIn(200);
        }, function () {
            $('#select-loan-button-over, #select-loan-button-over-small').fadeOut(200);
        });
    });
</script>

<script type="text/javascript" src="/themes/creditstar_ee//js/front-banner.js"></script>
</div><!-- content -->

</div>
<div id="footer" class="hide-on-print">
    <div class="row">
        <div class="medium-3 medium-push-5 columns">
            <div class="row special-dots" id="index-container">
                <h2 class="c-text-white">
                    <span class="show-for-medium-up">Sisupuu</span>
                    <span class="show-for-small">
                        <a class="toggle-content-tree" id="toggle-small-index" href="javascript:void(0);">
                            Sisupuu
                        </a>
                    </span>
                </h2>
                <ul class="no-bullet links-block">
                    <li><a href="/est/site/laenud">Laenud</a></li>
                    <li class="sublink"><a href="/est/site/laenud">Tutvustus</a></li>
                    <li class="sublink"><a href="/est/site/laenud/sub/tingimused">Tingimused</a></li>

                    <li><a href="/est/site/help">Vajab abi?</a></li>
                    <li class="sublink"><a href="/est/site/help">ID-punktid</a></li>
                    <li class="sublink"><a href="/est/site/help/sub/kkk">KKK</a></li>

                    <li><a href="/est/site/firmast">Firmast</a></li>
                    <li><a href="/est/site/vastutustundlik">Vastutustundlik laenamine</a></li>

                    <li class="sublink"><a href="/est/site/kupsis">Küpsiste kasutamise põhimõtted</a></li>
                </ul>
            </div>
        </div>
        <div class="medium-5 medium-pull-3 columns">
            <div class="row special-dots">
                <h2>Creditstar Group</h2>

                <div class="medium-10 columns">
                    <div class="row">
                        <p>Creditstar Estonia AS kuulub online krediidi ja pangavälise väikefinantseerimise valdkondades tegutsevasse rahvusvahelisse finantstehnoloogia gruppi Creditstar Group.</p>
                    </div>
                </div>

                <div class="awards"><img src="/themes/creditstar_ee//img/front-awards.png" alt=""/></div>
                <ul class="contacts no-bullet">
                    <li>© 2016 Creditstar Estonia AS</li>
                    <li class="contacts-address">Lõõtsa 5, Tallinn 11415</li>
                    <li class="contacts-phone">Klienditeenindus +372 644 0467</li>
                    <li class="contacts-phone">Infotelefon 1715*</li>
                    <li class="contacts-email">Email: info@creditstar.ee</li>
                    <li class="contacts-times">E-P 9.00-21.00</li>
                </ul>
                <ul class="accounts no-bullet">
                    <li><b>Muudatus pangakontodes! Pöörame tähelepanu, et alates 06.09.2019 saab tagasimakseid teha ainult SEB arvelduskontole nr EE701010220059578010. Teiste pankade kontodele palume tagasimakseid mitte teha.</b></li>
                </ul>
            </div>
        </div>
        <div class="medium-2 columns special-dots">
            <h2 class="c-text-white"><!--Pangad--></h2>

            <div>
                <!--<img src="/themes/creditstar_ee//img/footer-banks.png" alt=""/>-->
            </div>
        </div>

        <div class="medium-2 columns special-dots security">
            <h2 class="c-text-white">Turvalisus</h2>

            <div>
                <img src="/themes/creditstar_ee//img/footer-security.png" alt=""/>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="medium-5 columns">
            <div class="row">
                <ul class="privacy no-bullet inline-list">
                    <li><a href="/est/site/laenud/sub/tingimused#p-privaatsus">PRIVAATSUS JA TURVALISUS</a></li>
                    <li><a href="/est/site/laenud/sub/tingimused#p-kasutustingimused">KASUTUSTINGIMUSED</a></li>
                </ul>
            </div>
        </div>
        <div class="medium-4 columns">
            <div class="row">
                <ul class="user-buttons no-bullet inline-list">
                                            <li><a class="button-orange button-small" href="/est/site/login"><img
                                    src="/themes/creditstar_ee//img/ico-login.png" alt=""
                                    style="padding-right: 6px;margin-top: -4px;"/>LOGI
                                SISSE</a></li>
                        <li><a class="button-orange button-small" href="/est/register/index">AVA KONTO</a></li>
                                    </ul>
            </div>
        </div>
        <div class="medium-3 columns"></div>
    </div>
    <div class="row">
        <div class="medium-12 columns disclaimer">
            <div class="row" style="padding-bottom:10px;">
                * Lühinumbrile helistades kehtib põhitariifist kõrgem tasu (Telia võrgust helistades mobiiltelefonilt 0,2278 €/min, lauatelefonilt helistades 0,0222 €/min, lisandub kõnealustustasu 0,0312 €/kord, Tele2 võrgust 0,23 €/min ja Elisa võrgust 0,3 €/min)
            </div>
            <div class="row">
                Tähelepanu! Iga laen on finantskohustus. Enne lepingu sõlmimist tutvu hoolikalt teenuse tingimustega ja
                vajadusel pea nõu spetsialistiga. <br/>
                Krediidi kulukuse määr on 56,00% aastas järgmistel näidistingimustel: krediidilimiit 2000,00 €,
                laenuperiood 12 kuud, fikseeritud intressimäär 45,30%.
                Laenates näidistingimustel 2000,00 € perioodiks 12 kuud, on ühe kuumakse suurus 210,33 €, tagasimaksete
                summa 2523,96 € ja tarbija poolt makstav kogusumma 2523,96 €.
            </div>
        </div>
    </div>
</div>

<div id="terms-modal" class="reveal-modal" data-reveal>
    <div class="reveal-modal-content"></div>
    <a class="close-reveal-modal">&#215;</a>
</div>

<div id="campaign-modal" class="reveal-modal" data-reveal>
    <div class="reveal-modal-content"></div>
    <a class="close-reveal-modal">&#215;</a>
</div>

<script src="/themes/creditstar_ee//js/vendor/jquery.nouislider.js?ver=7.0.9"></script>
<script src="/themes/creditstar_ee//js/vendor/addtohomescreen.js?ver=1.1"></script>
<script src="/themes/creditstar_ee//js/foundation.min.js"></script>
<script src="/themes/creditstar_ee//js/scripts.js?ver=1.04"></script>
<script src="/themes/creditstar_ee//js/fslide.js"></script>
<!--[if lt IE 9]>
<script src="/themes/creditstar_ee/js/foundation-ie8.js"></script>
<![endif]-->
<script>
    $(document).foundation();
    function scrollToTop() {
        $("html, body").animate({ scrollTop: "1px" });
    }
</script>
<!-- SMS Laen FB retargeting EST -->
<script>(function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
        }
        _fbq.push(['addPixelId', '1396115694000697']);
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" border="0" alt="" style="display:none" src="https://www.facebook.com/tr?id=1396115694000697&amp;ev=NoScript" /></noscript>

<!-- end SMS Laen FB retargeting EST -->

<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 999935329;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/999935329/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>
<!-- end Google Code for Remarketing Tag -->

    <!-- Adform Tracking Code BEGIN -->
    <script type="text/javascript">
        window._adftrack = Array.isArray(window._adftrack) ? window._adftrack : (window._adftrack ? [window._adftrack] : []);
        window._adftrack.push(
            { pm: 1010521, divider: encodeURIComponent('|'), pagename: encodeURIComponent('Creditstar / trackingpoint') }
        );
        (function ()
            { var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://track.adform.net/serving/scripts/trackpoint/async/'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }
        )();
    </script>
    <noscript>
        <p style="margin:0;padding:0;border:0;">
            <img src="https://track.adform.net/Serving/TrackPoint/?pm=1010521&ADFPageName=Creditstar%20%2F%20trackingpoint&ADFdivider=|" width="1" height="1" alt="" />
        </p>
    </noscript>
    <!-- Adform Tracking Code END -->
<script>
    $(function () {
        $('#cookie-notice .close-icon').click(function(){
            $('#cookie-notice').hide();
            $.cookie('cookie_policy_accepted', '1', { expires: 365, path: '/' });
        });

        $('#iphone').on('click', function (e) {
            e.preventDefault();
            var frame = '<iframe src="/est/site/iphoneCampaignRules" frameborder="0"></iframe>';
            $('#campaign-modal .reveal-modal-content').html(frame);
            $('#campaign-modal').foundation('reveal', 'open');
        });
        $('#kinkekaart').on('click', function (e) {
            e.preventDefault();
            var frame = '<iframe src="/est/site/PartnerkaartCampaignRules" frameborder="0"></iframe>';
            $('#campaign-modal .reveal-modal-content').html(frame);
            $('#campaign-modal').foundation('reveal', 'open');
        });
        $('#campain_viisakus_est').on('click', function (e) {
            var win = window.open("https://www.viisakusskoor.ee/?utm_source=cs_index&utm_medium=index&utm_campaign=viisakusskoor", '_blank');
            win.focus();

        });
    });
</script>
<script src="/themes/creditstar_ee/js/jquery.cookie.js"></script>

<script type="text/javascript" src="https://code3.adtlgc.com/js/egrupp_ar.js"></script>
<script type="text/javascript"><!--<![CDATA[
    /* (c)AdOcean 2003-2016 */
    /* PLACEMENT: neti_ee.retargeting.aktiveerimine.creditstar_1 */
    if(location.protocol.substr(0,4)=='http')document.write(unescape('%3C')+'script id="neti_ee.retargeting.aktiveerimine.creditstar_1" src="'+location.protocol+'//netiee.adocean.pl/_'+(new Date()).getTime()+'/ad.js?id=39Hx8ImADAw9_ADtyoRB_pLDXZx69cedYV8lzj9OUrT.V7/x='+screen.width+'/y='+screen.height+'" type="text/javascript"'+unescape('%3E%3C')+'/script'+unescape('%3E'));
    //]]>-->
</script>

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 8486737;
    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>
<!-- End of LiveChat code -->
</body>
</html>
