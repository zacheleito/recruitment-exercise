<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/** @var app\models\User $users */
/** @var yii\data\Pagination $pages */
/** @var app\models\UserSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'List of users';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-index">

    <?php if (Yii::$app->session->hasFlash('message')): ?>
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?= Yii::$app->session->getFlash('message'); ?>
        </div>
    <?php endif; ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <hr>
        <span><?= Html::a('Add User', ['/user/create'], ['class' => 'btn btn-primary pull-right']) ?></span>
        <a href="<?= yii::$app->homeUrl ?>" class="btn btn-default">Back</a> <hr>
        
    </div>
    <div class="row">
        <?php Pjax::begin(); ?>
        
        <?=

        GridView::widget([

            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'first_name',
                'last_name',
                'email',
                [
                    'attribute' => 'Birth Date',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->getBirthDate()->format('d M Y');
                    },
                ],
                [
                    'attribute' => 'Age',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->age()->y;
                    },
                ],
                'personal_code',
                'phone',
                'active:boolean',
                'dead:boolean',
                'lang',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{leadCreate} {leadView} {leadUpdate} {leadDelete}',
                    'buttons' => [
                        'leadCreate' => function ($url, $model) {
                            $url = Url::to(['loan/create', 'user_id' => $model->getAttribute('id')]);
                            return Html::a('Crate a Loan for user', $url, ['title' => 'view', 'class' => 'label label-success']);
                        },
                        'leadView' => function ($url, $model) {
                            $url = Url::to(['user/view', 'id' => $model->getAttribute('id')]);
                            return Html::a('View', $url, ['title' => 'view', 'class' => 'label label-primary']);
                        },
                        'leadUpdate' => function ($url, $model) {
                            $url = Url::to(['user/update', 'id' => $model->getAttribute('id')]);
                            return Html::a('Edit', $url, ['title' => 'update', 'class' => 'label label-default']);
                        },
                        'leadDelete' => function ($url, $model) {
                            $url = Url::to(['user/delete', 'id' => $model->getAttribute('id')]);
                            return Html::a('Delete', $url, [
                                'title' => 'delete',
                                'class' => 'label label-danger',
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this user?'),
                                'data-method' => 'post',
                            ]);
                        },
                    ]],
            ],
        ]); ?>
        
        <?php Pjax::end(); ?>
        
        

    </div>

</div>

