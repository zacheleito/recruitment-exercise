<?php

use yii\helpers\Html;

/** @var app\models\User $user_model */

$this->title = 'View User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['/user/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Name:
            <?=
            $user_model->getAttribute('first_name')
            . ' ' .
            $user_model->getAttribute('last_name')
            ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Email:
            <?= $user_model->getAttribute('email') ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Birth Date:
            <?= $user_model->getBirthDate()->format('d M Y') ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Age:
            <?= $user_model->age()->y ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Personal Code:
            <?= $user_model->getAttribute('personal_code') ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Phone:
            <?= $user_model->getAttribute('phone') ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Status:
            <?= $user_model->getAttribute('active') ? 'Active' : 'Inactive'; ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Vital Status:
            <?= $user_model->getAttribute('dead') ? 'Dead' : 'Alive'; ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Language:
            <?= $user_model->getAttribute('lang') ?>
        </li>
    </ul>
    <div class="row">
        <div class="col-lg-1">
            <?php Html::a('Add Loan', ['loan/create', 'user_id' => $user_model->getAttribute('id')], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="col-lg-1">
            <?= Html::a('Edit', ['update', 'id' => $user_model->getAttribute('id')], ['class' => 'btn btn-default']) ?>
        </div>
        <div class="col-lg-3">
            <?= Html::a('Delete', ['delete', 'id' => $user_model->getAttribute('id')], ['class' => 'btn btn-danger']) ?>
        </div>

        <div class="col-lg-7">
            <?= Html::a('Back', ['user/index'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>


</div>