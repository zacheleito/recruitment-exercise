#Test exercise for Creditstar Group

## Demo
https://bit.ly/2kLCon7

## Features
This is a webapp that provides:
1.  Viewing, adding, editing and removing Loans and Users in the database. User's input are validated.
2.  Listing out all the Loans and Users (pagination, filtering, and sorting).
3.  Data imported from JASON files into the database programmatically.
4.  Can get user age from user personal code. 
5.  Style of the page is based on ```recruitment.png``` file.

## Setup
1. Clone the repository to your local server directory.
   git clone https://zacheleito@bitbucket.org/zacheleito/recruitment-exercise.git
2. run: composer install
3. create your local database.
4. Update config/db.php file with your database credentials 
5. run: php yii migrate
6. run: ./yii serve and access the app through http://localhost:8080/

#DONE