<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{
    public function rules()
    {
        // only fields in rules() are searchable
        return [
            [['first_name', 'last_name', 'email', 'lang'], 'safe'],
            [['phone', 'personal_code'], 'integer'],
            [['active', 'dead'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['personal_code' => $this->personal_code]);
        $query->andFilterWhere(['lang' => $this->lang]);
        $query->andFilterWhere(['dead' => $this->dead]);
        $query->andFilterWhere(['active' => $this->active]);

        return $dataProvider;
    }
}