<?php

namespace app\models;

use app\components\validators\PersonalCodeValidator;
use DateTime;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $personal_code
 * @property int $phone
 * @property bool $active
 * @property bool $dead
 *
 * @property Loan $loan
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'active' => 'Status',
            'dead' => 'Vital Status',
            'lang' => 'Language',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // The first_name, last_name, email and personal_code attributes are required
            [['first_name', 'last_name', 'email', 'personal_code', 'phone', 'active', 'dead'], 'required'],

            // The first_name, last_name and lang attribute should be string
            [['first_name', 'last_name', 'lang'], 'string'],

            // The personal_code and phone attribute should be integer
            [['personal_code', 'phone'], 'integer'],

            // The active and dead attribute should be boolean
            [['active', 'dead'], 'boolean'],

            // The email attribute should be a valid email address
            [['email'], 'email'],

            ['personal_code', PersonalCodeValidator::className()]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * Get user birthday as Datetime object
     *
     * @return Datetime user birthday
     * @throws \Exception
     */
    public function getBirthDate()
    {
        $year = substr($this->personal_code, 1, 2);
        $month = substr($this->personal_code, 3, 2);
        $day = substr($this->personal_code, 5, 2);
        $birthDate = new Datetime($year . '-' . $month . '-' . $day);
        return $birthDate;
    }

    /**
     * Get user birthday as Datetime object
     *
     * @return bool|\DateInterval user birthday
     * @throws \Exception
     */
    public function age()
    {
        $now = new DateTime();
        return $this->getBirthDate()->diff($now);
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['user_id' => 'id']);
    }

    /**
     * @param integer $id
     * @return User|null
     * @throws NotFoundHttpException
     * @author Abdelatif Zache <zacheabdelatif@outlook.de>
     */
    public static function findOneOrFail($id)
    {
        $user = self::findOne($id);

        if ($user !== null) {
            return $user;
        }

        throw new NotFoundHttpException('User Not Found.');
    }

}
