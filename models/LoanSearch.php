<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class LoanSearch extends Loan
{
    public function rules()
    {
        // only fields in rules() are searchable
        return [
            [['start_date', 'end_date'], 'safe'],
            [['amount', 'interest', 'campaign', 'duration'], 'integer'],
            [['status'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Loan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['start_date' => $this->start_date]);
        $query->andFilterWhere(['end_date' => $this->end_date]);
        $query->andFilterWhere(['amount' => $this->amount]);
        $query->andFilterWhere(['duration' => $this->duration]);
        $query->andFilterWhere(['campaign' => $this->campaign]);
        $query->andFilterWhere(['interest' => $this->interest]);
        $query->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }
}