<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m190913_130321_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $users = $this->readJson('users.json');

        foreach ($users as $user) {

            $this->insert('user', [
                "id" => $user['id'],
                "first_name" => $user['first_name'],
                "last_name" => $user['last_name'],
                "email" => $user['email'],
                "personal_code" => $user['personal_code'],
                "phone" => $user['phone'],
                "active" => $user['active'],
                "dead" => $user['dead'],
                "lang" => $user['lang'],
            ]);

        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $users = $this->readJson('users.json');

        foreach ($users as $user) {

            $this->delete('user', ['id' => $user['id']]);

        }

    }

    private function readJson($file)
    {
        $url = Yii::$app->basePath . '/' . $file;
        return json_decode(
            file_get_contents($url),
            true
        );
    }
}
