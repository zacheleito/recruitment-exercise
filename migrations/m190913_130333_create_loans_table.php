<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%loans}}`.
 */
class m190913_130333_create_loans_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $loan = $this->readJson('loans.json');

        foreach ($loan as $loan) {

            $this->insert('loan', [
                "id" => $loan['id'],
                "user_id" => $loan['user_id'],
                "amount" => $loan['amount'],
                "interest" => $loan['interest'],
                "duration" => $loan['duration'],
                "start_date" => $this->timestampToDatetime($loan['start_date']),
                "end_date" => $this->timestampToDatetime($loan['end_date']),
                "campaign" => $loan['campaign'],
                "status" => $loan['status'],
            ]);

        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $loans = $this->readJson('loans.json');

        foreach ($loans as $loan) {

            $this->delete('loan', ['id' => $loan['id']]);

        }
    }

    private function readJson($file)
    {
        $url = Yii::$app->basePath . '/' . $file;
        return json_decode(
            file_get_contents($url),
            true
        );
    }

    /**
     * @param $timestamp int
     * @return string
     * @throws Exception
     */
    private function timestampToDatetime($timestamp)
    {
        $time = new \DateTime("@$timestamp");
        return $time->format('Y-m-d H:i:s');
    }
}
