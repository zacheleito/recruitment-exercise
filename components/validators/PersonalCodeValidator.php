<?php

namespace app\components\validators;

use yii\validators\Validator;

/**
 * Class PersonalCodeValidator
 */
class PersonalCodeValidator extends Validator
{

    // Regex to validate personal code
    const PERSONAL_CODE_REGEX = '/^[1-6][0-9]{2}[0-1][0-9][0-9]{2}[0-9]{4}$/';

    public function validateAttribute($model, $attribute)
    {

        // Personal code is 11 digits long
        if (strlen($model->$attribute) !== 11) {
            $this->addError($model, $attribute, 'The personal code must be 11 digits long.');
        }
        // Validate against regex
        if (!preg_match(self::PERSONAL_CODE_REGEX, $model->$attribute)) {
            $this->addError($model, $attribute, 'The personal code is invalid.');
        }

    }

}